# build sqlite with meson

This repository contains a Meson build definitions for sqlite project.

In contrary to one found at https://github.com/mesonbuild/sqlite, this builds
correctly with MSVC, generating import library. The only difference is additional
project argument that defines 'SQLITE_API' as '__declspec(dllexport)'.
